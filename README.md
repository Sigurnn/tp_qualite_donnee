# TP_Qualite_Donnee

# Sujet du TP
Comparer deux jeux de données de climat pour déterminer la capitale européenne dont les données de température sont fournies dans le fichier Climat.xlsx 
On se servira du fichier Savukoskikirkonkyla.xlsx issu de l’open data pour servir de référence.

**Objectifs :**

Mettre en oeuvre un environnement de traitement graphique de données issues de sources plus ou moins fiables.
Corriger un jeu de données mal formé
proposer un candidat potentiel pour l’origine des données.
Déroulement :

Pour l’échantillon SI, calculez :
 - Moyenne par mois
 - Ecart type par mois
 - Min /max par mois et par année
 - Utiliser par Python Scipy pour les parties mathématiques.
 - Tracer les courbes de chaque mois avec une bibliothèque graphique python Matplotlib, 12 vues mensuelles
 - Assembler les courbes sur un seul graphique (J1 -> J365) : vue annuelle
 - Présenter la valeur lue en parcourant la courbe à l'aide du pointeur
 - Présenter les valeurs précédentes par mois glissant de 30 jours centré sur la valeur lue

_Création du dataframe :_

```
    df = pd.read_excel('./data/Climat.xlsx',usecols=range(3,15),skiprows=4,skipfooter=17, header=None)
    df.columns = all_months
    return df
```


_Obtention des moyennes par mois :_

```
    print("-----Moyenne par mois-----")
    print(df.mean())
```
_Résultats :_

    Janvier      -8.967742
    Février      -4.928571
    Mars          1.193548
    Avril         8.000000
    Mai          12.903226
    Juin         17.033333
    Juillet      18.322581
    Août         17.838710
    Septembre    10.500000
    Octobre       3.870968
    Novembre     -0.333333
    Décembre     -7.000000

_Obtention des écart-types par mois :_

```
    print("-----Ecart type par mois-----")
    print(df.std())
```
_Résultats :_

    Janvier      4.490611
    Février      4.561746
    Mars         3.506369
    Avril        3.151354
    Mai          4.036127
    Juin         3.011281
    Juillet      3.571994
    Août         2.696872
    Septembre    2.330458
    Octobre      1.802627
    Novembre     3.717000
    Décembre     2.065591

_Obtention des minimum et maximum par mois et par année :_

```
    print("-----Minimum par mois-----")
    print(df.min())
    return df.min()

    print("-----Maximum par mois-----")
    print(df.max())
    return df.max()

    print("-----Minimum de l'année-----")
    print(minimum.min())
    print("-----Maximum de l'année-----")
    print(maximum.max())
```
_Résultats :_
    
    -----Minimum par mois-----
    Janvier     -23.0
    Février     -12.0
    Mars         -8.0
    Avril         2.0
    Mai           5.0
    Juin         11.0
    Juillet      13.0
    Août         14.0
    Septembre     6.0
    Octobre       1.0
    Novembre     -8.0
    Décembre    -11.0

    -----Maximum par mois-----
    Janvier      -3.0
    Février       5.0
    Mars          7.0
    Avril        16.0
    Mai          18.0
    Juin         22.0
    Juillet      26.0
    Août         26.0
    Septembre    16.0
    Octobre       7.0
    Novembre      5.0
    Décembre     -3.0

    -----Minimum de l'année-----
    -23.0

    -----Maximum de l'année-----
    26.0

_Obtention des courbes par mois :_
 
```
    for col in df :    
        plt.plot(df[col].dropna(), label=col)
        mplcursors.cursor(hover=True)
        plt.xlabel('Jour')
        plt.ylabel('Température (°C)')
        plt.legend()
        plt.show()
```


_Courbe obtenue (exemple pour le mois de janvier) :_

![](img/monthGraph.PNG)

_Obtention de la courbe annuelle :_

```
    array = []
    for col in df :
        array = [*array, *df[col].dropna()]   
    plt.plot(array)
    mplcursors.cursor(hover=True)
    plt.xlabel('Jour')
    plt.ylabel('Température (°C)')
    plt.legend()
    plt.show()
```


_Courbe obtenue :_

![](img/annualGraph.PNG)

**Recommencez avec le jeu SI-erreur après avoir corrigé les valeurs en erreur. Précisez vos méthodes.**
 - Création d'un dataframe à partir du CSV
 - Analyse des valeurs différentes de int ou égale à string
 - Remplacement des valeurs d'erreur (string)
 - Utilisation du dataframe corrigé

Pour le jeux de données comprenant des erreurs, j'ai utilisé la même démarche que pour celle composé d'erreur. A la différence qu'une fonction y a été ajoutée pour remplacer les erreurs par des valeurs.

Pour calculer des valeurs corrigées, j'ai fait la moyenne de la valeurs précédentes et suivantes.

            > (Jour précédent + Jour suivant) / 2

_Correction des erreurs :_

```
    df = pd.read_excel('./data/Climat.xlsx',usecols=range(3,15),skiprows=4,skipfooter=22, header=None, sheet_name=1)
    for column in range(len(df.count())):
        for row in range(len(df.iloc[:, column])):
            if (type(df.iloc[row, column]) is str) :
                df.iloc[row, column] = (df.iloc[row - 1, column] + df.iloc[row + 1, column])/2
    df.columns = all_months
    return df
```

La fonction n'est pas très complexe et ne va pas vérifier si les valeurs (précédentes et suivantes) sont correctes avant de les utiliser dans le calcul car j'ai réalisé une phase d'analyse au préalable pour vérifier que ce cas n'existait pas.


**Les données corrigées sont elles proches des valeurs sans erreur ?**

            | Valeurs Réelles  | Valeurs Corrigées |
    Mars    |       3          |        4.5        |
    Juin    |       15         |        14         |                                
    Juillet |       21         |        21.5       |
    Octobre |       5          |        3.5        |



Les valeurs corrigées varie de plus ou moins 1.5 degrés ce qui reste acceptable en valeur de substitution mais les valeurs pourraient être plus précise avec d'autres calculs.

**A partir de données opendata du second fichier, retrouver le type de climat reprendre les données typiques de la localisation proche fournies en complément , comparer les écarts.**
 
Le jeux de données utilisé est un jeux de température récupéré sur Kaggle (site de défi sur l'IA) : [Kaggle](https://www.kaggle.com/sudalairajkumar/daily-temperature-of-major-cities).

_Création d'un graphe comparatif entre notre ville inconnu et la ville (Russe) :_

![](img/ruskov.PNG)

En analysant l'excel et l'image ci-dessus, on constate que le climat est tempéré (mais continental : c'est à dire situé dans les terres). C'est à dire avec des température basse et qui peuvent descendre vers de grandes valeurs négatives.

Le jeux de données comporte plus d'un million de lignes. Il a donc fallu les filtrer.

_Filtrage des données :_

```
    capitales = capitales[capitales.Region == 'Europe']
    capitales = capitales[capitales.Year == 2018]
    capitales["AvgTemperature"] = (capitales["AvgTemperature"] - 32)/1.8
    print(capitales)
    return capitales
```

Ensuite, il faut comparer les données à notre ville inconnu.

_Recherche d'une ville compatible :_

```
     classement = {}
    for capitale in capitales['City'].unique():
        temperatures = capitales.loc[capitales['City'] == capitale]
        difference = 0
        for month in capitales['Month'].unique():
            difference += abs(float(temperatures['AvgTemperature'].loc[temperatures['Month'] == month].mean()) - float(villeInconnu[dicoMonth.get(month)].mean()))
        classement[capitale] = difference
    sortedClassement = sorted(classement, key=classement.get)
    for i in range(4):
        print("N°" + str(i) + " : " + sortedClassement[i])

```

Les villes les plus proches (classées dans l'ordre) sont :

    N°0 : Moscow - 30.339755404626857
    N°1 : Oslo - 32.72959454590279
    N°2 : Riga - 38.36934203789042
    N°3 : Helsinki - 39.07184767973298
    N°4 : Stockholm - 40.99262246117085
    N°5 : Minsk - 44.868512753890656
    N°6 : Copenhagen - 48.58636191234757
    N°7 : Kiev - 48.66412925982818
    N°8 : Munich - 56.022933493538595
    N°9 : Prague - 56.449862509719125
    N°10 : Bern - 58.13360989969257
    N°11 : Warsaw - 58.57738190058977
    N°12 : Zurich - 63.9932137674691
    N°13 : Dublin - 64.25251038288673
    N°14 : Belfast - 64.41210120461758
    N°15 : Reykjavik - 66.930904447361
    N°16 : Amsterdam - 69.1805189514249
    N°17 : Sofia - 70.37504930685932
    N°18 : Brussels - 70.42986525952475
    N°19 : Vienna - 75.21009984639016
    N°20 : London - 76.45745813658
    N°21 : Budapest - 76.85736141927897
    N°22 : Bratislava - 78.1860347803
    N°23 : Geneva - 78.79044347727142
    N°24 : Bucharest - 80.2542498719918
    N°25 : Paris - 82.7870711894204
    N°26 : Skopje - 90.19185962716428
    N°27 : Zagreb - 93.49060656919079
    N°30 : Bordeaux - 101.97889144905274
    N°31 : Madrid - 111.63180671711136
    N°33 : Lisbon - 126.36603705600122
    N°34 : Tirana - 131.22744160504942
    N°35 : Rome - 134.34763896000456
    N°36 : Barcelona - 135.28554692685518

**Qu'en concluez vous ?**

La ville qui constitue notre jeux de données est une ville d'europe du nord ou de l'est. et que notre supposition de départ est juste.

**De quelle la capitale européenne avez vous eu les données.**

_Comparaison visuelle (ville inconnu / ville du top 3) :_

```
    array = [] 
    cityName=["Moscow","Riga","Oslo","Helsinki","Stockholm"]
    for col in villeInconnu :
        array = [*array, *villeInconnu[col].dropna()]   
    for i in cityName :
        city = capitales[capitales.City == i]
        array2 = list(city['AvgTemperature'])
        plt.plot(array,label='Ville inconnu', color='black')
        plt.plot(array2,label=i, color='red')
        mplcursors.cursor(hover=True)
        plt.xlabel('Jour')
        plt.ylabel('Température (°C)')
        plt.legend()
        plt.show()
```

_Graphe de comparaison avec Moscow :_
![](img/moscow.PNG)

_Graphe de comparaison avec Riga :_
![](img/rigo.PNG)

_Graphe de comparaison avec Oslo :_
![](img/oslo.PNG)


Avec le jeux de données kaagle, on peut observer que les trois premières villes ont des valeurs très proches de notre courbe.
Pour autant, aucune d'entre elles ne se calque parfaitement sur notre ville inconnu.
Donc la capitale européenne est l'une des trois voir cinq premières ville de notre classement.
C'est à dire : **Moscow, Oslo, Riga, Helsinki ou Stockholm**


**Outils :**
à utiliser Python + matplotlib, Jupyter éventuellement. Pas de R ni d’autre langage autorisés Evaluation: Démonstration des solutions techniques et argumentation sur les méthodes utilisées
