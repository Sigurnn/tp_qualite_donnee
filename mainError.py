import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import mplcursors

all_months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']

def main():
    csvToDataframe()
    print(csvToDataframe())
    monthAverage(csvToDataframe())
    monthDeviation(csvToDataframe())
    monthMin(csvToDataframe())
    monthMax(csvToDataframe())
    yearMinMax(monthMin(csvToDataframe()),monthMax(csvToDataframe()))
    monthlyGraphs(csvToDataframe())
    annualGraph(csvToDataframe())

def csvToDataframe():

    df = pd.read_excel('./data/Climat.xlsx',usecols=range(3,15),skiprows=4,skipfooter=22, header=None, sheet_name=1)
    for column in range(len(df.count())):
        for row in range(len(df.iloc[:, column])):
            if (type(df.iloc[row, column]) is str) :
                df.iloc[row, column] = (df.iloc[row - 1, column] + df.iloc[row + 1, column])/2
    df.columns = all_months
    return df

def monthAverage(df):

    print("-----Moyenne par mois-----")
    print(df.mean())

def monthDeviation(df):

    print("-----Ecart type par mois-----")
    print(df.std())

def monthMin(df):

    print("-----Minimum par mois-----")
    print(df.min())
    return df.min()

def monthMax(df):

    print("-----Maximum par mois-----")
    print(df.max())
    return df.max()

def yearMinMax(minimum,maximum):

    print("-----Minimum de l'année-----")
    print(minimum.min())
    print("-----Maximum de l'année-----")
    print(maximum.max())
    

def monthlyGraphs(df):
    
    for col in df :    
        plt.plot(df[col].dropna(), label=col)
        mplcursors.cursor(hover=True)
        plt.xlabel('Jour')
        plt.ylabel('Température (°C)')
        plt.legend()
        plt.show()


def annualGraph(df):
    array = []
    for col in df :
        array = [*array, *df[col].dropna()]   
    plt.plot(array)
    mplcursors.cursor(hover=True)
    plt.xlabel('Jour')
    plt.ylabel('Température (°C)')
    plt.legend()
    plt.show()
   
main()