import matplotlib.pyplot as plt
import pandas as pd
import mplcursors

all_months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
dicoMonth = {
    1: "Janvier",
    2: "Février",
    3: "Mars",
    4: "Avril",
    5: "Mai",
    6: "Juin",
    7: "Juillet",
    8: "Août",
    9: "Septembre",
    10: "Octobre",
    11: "Novembre",
    12: "Décembre"
}

def main() :

    csvCityToDataFrame()
    csvCapitaleToDataFrame()
    csvRuskovCityToDataFrame()
    print(csvRuskovCityToDataFrame())
    print(csvCapitaleToDataFrame())
    filterDataFrame(csvCapitaleToDataFrame())
    findClosestData(filterDataFrame(csvCapitaleToDataFrame()),csvCityToDataFrame())
    graphRuskov(csvRuskovCityToDataFrame(),csvCityToDataFrame())
    graph(filterDataFrame(csvCapitaleToDataFrame()),csvCityToDataFrame())

def csvCityToDataFrame() :

    df_villeInconnu = pd.read_excel('./data/Climat.xlsx',usecols=range(3,15),skiprows=4,skipfooter=17, header=None)
    df_villeInconnu.columns = all_months
    return df_villeInconnu

def csvCapitaleToDataFrame() :

    capitales = pd.read_csv('./data/city_temperature.csv')
    return capitales

def csvRuskovCityToDataFrame() :

    capitales = pd.read_excel('./data/city.xlsx',usecols=range(5,6),skiprows=1, header=None,  sheet_name=2)
    return capitales

def filterDataFrame(capitales) :
    capitales = capitales[capitales.Region == 'Europe']
    capitales = capitales[capitales.Year == 2018]
    capitales = capitales.loc[capitales['AvgTemperature'] != -99.0]
    capitales["AvgTemperature"] = (capitales["AvgTemperature"] - 32)/1.8
    return capitales

def findClosestData(capitales,villeInconnu) :
    classement = {}
    for capitale in capitales['City'].unique():
        temperatures = capitales.loc[capitales['City'] == capitale]
        difference = 0
        for month in capitales['Month'].unique():
            difference += abs(float(temperatures['AvgTemperature'].loc[temperatures['Month'] == month].mean()) - float(villeInconnu[dicoMonth.get(month)].mean()))
        classement[capitale] = difference
    sortedClassement = sorted(classement, key=classement.get)
    sortedValue = sorted(classement.values())
    print(sortedValue)
    for i in range(len(sortedClassement)):
        print("N°" + str(i) + " : " + sortedClassement[i]+" - "+str(sortedValue[i]))
    print(sortedClassement)
    return sortedClassement

def graphRuskov(ruskov,villeInconnu) :
    array = [] 
    array2 = [] 
    for col in villeInconnu :
        array = [*array, *villeInconnu[col].dropna()]   
    plt.plot(array,label='Ville inconnu', color='black')
    plt.plot(ruskov[5],label="Ruskov", color='red')
    mplcursors.cursor(hover=True)
    plt.xlabel('Jour')
    plt.ylabel('Température (°C)')
    plt.legend()
    plt.show()

def graph(capitales,villeInconnu) :
    array = [] 
    cityName=["Moscow","Riga","Oslo","Helsinki","Stockholm"]
    for col in villeInconnu :
        array = [*array, *villeInconnu[col].dropna()]   
    for i in cityName :
        city = capitales[capitales.City == i]
        array2 = list(city['AvgTemperature'])
        plt.plot(array,label='Ville inconnu', color='black')
        plt.plot(array2,label=i, color='red')
        mplcursors.cursor(hover=True)
        plt.xlabel('Jour')
        plt.ylabel('Température (°C)')
        plt.legend()
        plt.show()

main()